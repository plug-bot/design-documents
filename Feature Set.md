# Feature Set
This document is aimed at being a compact overview of what Plug will offer
## Plugins
Plugins are the central piece of the Plug ecosystem. They can be written by anyone. They are hosted and highly managed in a kubernetes cluster
### Features
Features are sort of sub-modules of a Plugin. They each describe something that the Plugin offers to do, and can be turned on or off separately
### Permissions
Permissions are requested by Plugins and granted or refused by the person setting up Plug for use in a server. They specify what parts of the Discord API Plugins can use, with what other Plugins can they communicate, and what Data can they access and if they can store additional one

## Compatibility
Compatibility with the existing Discord Bot ecosystem is a key component of the Plug architecture and ecosystem. The internal API used by Plugins to control Plug and use its features is a superset of Discord's, meaning it should work out of the box with existing Discord libraries

## Internal API
Plug exposes an API to Plugins. It :
- Adds a security layer around Discord's API to ensure that Plugins do not have access to guilds they haven't been activated and given permissions in
- Exposes a feature that enables Plugin makers to easily store and fetch data
- Enables Inter-Plugin Communication

## Restricted Resources
Not exactly a feature, but it is important to note that Plugins will have relatively heavily restricted resources. How limited depends on the growth of Plug