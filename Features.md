# Features

## Overview

Features are a functionality provided by Plug to slice different parts of a Plugin into boxes. What that means is that Plugins can declare different Features, each representing an actual feature of the Plugin. For example, an administration Plugin might declare muting members as a Feature

Server Admins managing Plug can enable and disable each Features of a Plugin

Each Feature is tied to a Permission Set, requiring the Permissions to be enabled for the Feature to be enabled as well