# Permissions
## Overview
Permissions describe what a Plugin is able to do

## Span of permissions
Permissions need to be specified for :
- Areas of the Discord APIs used
- Inter Plugin Communication
- Data access