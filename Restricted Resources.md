# Restricted Resources
## Overview
Each Plugin will have access to restricted resources. Included resources are
- CPU time
- Memory usage
- Size of stored data
Restrictions will be per-plugin for both CPU time and memory usage  
Restrictions for storage capacity will be per-guild basis. Default storage space limits will be set evenly for all enabled plugins or according to [Declarations](#resource-usage-declaration) if implemented. Discord Admins will be able to modify those limits themselves. May be blocked behind [Monetisation](Monetisation.md)  
Behaviour when getting to the limit of data storage is TBD

## Elasticity
Resources may be elastic, that is to say resource usage would be monitored and lowered if under a compsumption threshold, and raised if above another threshold, if resources are available  
This can easily be achieved by hosting the Bot on Kubernetes, which is planned

### Resource Usage Declaration
Plugin makers would ideally declare how demanding their plugin is. Plugins declared as needing less than default resources would be started at the stated needed resources, or a predefined global resource limit depending on if we ask for resource usage estimations from the Plugin makers, or simply a "Low/Medium/High" scale  
Starting resources of Plugins stated to be computation-intensive will most probably start with normal resources, the elasticity of distrubuted resources will take care of providing more of them if needed and available

## Data Access
Frequency of access to the data may be a potential resource restriction if it starts to be a problem at infrastructure level

## Monetisation
Resources may be the main point of monetisation for Plug. More in [Monetisation](Monetisation.md)