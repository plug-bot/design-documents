# Monetisation
## Overview
Monetisation will be needed to keep the project afloat. Resource demands of plugins and number of plugins will grow with popularity, and that means infrastructure to support it. Infrastructure costs money

The current idea is to have the main focus be on enabling clients to improve performance of the Plugins they use.  
All services will require recurring payments

## Transparency
Monetisation of this project is made not to be profitable to its creator, but to support its existence. While margins will be made on the sold services, it can be expected of the generated monetary resources to mostly be reinjected in the project. 

## Improving Plugin Performance
There are two ways a client can improve the peformance of the plugin they use :
- Pay for resources to be added to the Plugin globally
- Pay for the hosting of a private replica of Plug, where only certain Plugins are enabled and those Plugins only process events for the specified guilds

### Global Resources
A client will be able to rent certain amounts to add to the global instance of a Plugin. This will benefit everyone using the Plugin. It is unclear at the moment what the minimum price and resource amount will be. We can assume that as the infrastucture and user base grows, those minimums will shrink

### Private Hosting
A client will be able to rent a hosted private instance of Plug, where only certain Plugins are enabled, those Plugins only processing relevant events for enabled guilds  
It is currently unclear what the minimum size of the rented platform will be, and how many plugins it will support. This will be known after testing the resource requirements of Plugins  
Hosting solutions will have a price margin that will go towards contributing to the global infrastructure  
Hosting will have multiple tiers. Lower tiers might cut down on some infrastructure features to be allowed to be run on smaller -and cheaper- hardware

### Storage space
Each guild has its own storage space. More storage space can be rented

## Paid Features
Paid features might be required if we think that users will no want to pay for better performance of the plugins they use. Below is a list of features to possibly be paid-only
- ### Redistribution of Plugin resources
    The space allocated to each Plugin in a guild's storage is distributed evenly by default. A paid feature could allow a server owner to redistribute their storage space to allow certain plugins to use more and others less
